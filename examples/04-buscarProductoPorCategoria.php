#!/usr/bin/php

<?php
include("../autoload.php");

echo "\n#### Busqueda Productos - Por Categoria";

$cliente_id='g9RlglEPR9Vbiw1lUmhmx4IYGpPfFNyR';
$secret='CVeS1ATJPgoOW7kLt8KaWnaXw4NgKQYag7dSuJov';
$sandbox=false; # true: habilitar
$debug=false; # modo debug

$app= new Syscom($cliente_id, $secret, $sandbox, $debug);

if( $app->getError() )
	echo "\nError: ". $app->getError();
else {
	echo "\n\n***************************\n";
	echo "Token: ". $app->getToken();
	echo "\n***************************\n";

	/**
	* buscando productos en Redes [26]
	*/

	echo "\n\nProducto:\n\n";

	# ref: https://developers.syscom.mx/docs#operation/B%C3%BAsqueda%20de%20productos
	$params= array(
		"categoria"=>26, 
		#"busqueda"=>"", 
		#"modelo"=>"", 
		#"pagina"=>1,
		#"agrupar"=>false, # boolean
		#"orden"=>"", # relevancia (defecto), precio:asc, precio:desc, modelo:asc, modelo:desc, marca:asc, marca:desc, topseller 
		#"marca"=>"",
	);
	$app->getProducto($params);
	if( $app->getError() )
		echo "\nError: ". $app->getError();
	else {
		echo "\n\nDatos:\n\n";
		print_r($app->getRespuesta());
	}
}

echo "\n\nFin del programa...\n";
?>