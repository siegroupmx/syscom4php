#!/usr/bin/php

<?php
include("../autoload.php");

echo "\n#### Listar Seccion - De Varios Niveles";

$cliente_id='g9RlglEPR9Vbiw1lUmhmx4IYGpPfFNyR';
$secret='CVeS1ATJPgoOW7kLt8KaWnaXw4NgKQYag7dSuJov';
$sandbox=false; # true: habilitar

$app= new Syscom($cliente_id, $secret, $sandbox);

if( $app->getError() )
	echo "\nError: ". $app->getError();
else {
	echo "\n\n***************************\n";
	echo "Token: ". $app->getToken();
	echo "\n***************************\n";

	$app->getCategoria();
	if( $app->getError() )
		echo "\nError: ". $app->getError();
	else {
		echo "\n\nCategorias:\n\n";
		print_r($app->getRespuesta());
	}

	# listando Redes, id: 26
	echo "\n\n> Redes\n";
	$app->getCategoria("/26");
	if( $app->getError() )
		echo "\nError: ". $app->getError();
	else {
		echo "\n\nCategorias:\n\n";
		print_r($app->getRespuesta());
	}

	# listando Redes/Herramientas, id: 26/287
	echo "\n\n> Redes/Herramientas\n";
	$app->getCategoria("/287");
	if( $app->getError() )
		echo "\nError: ". $app->getError();
	else {
		echo "\n\nCategorias:\n\n";
		print_r($app->getRespuesta());
	}
}

echo "\n\nFin del programa...\n";
?>