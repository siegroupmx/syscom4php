<?php
/**
* syscom4Php
* SDK PHP para conectar al webservice de syscom.mx
*
* @author Angel Cantu Jauregui
* @author angel.cantu@sie-gorup.net
* @link https:/www.sie-group.net
* @version 1.1
* @license GNU GENERAL PUBLIC LICENSE, Version 3, 29 June 2007
*
*/

include( "src/Syscom.php" );
?>
