<?php
/**
* syscom4Php
* SDK PHP para conectar al webservice de syscom.mx
*
* @author Angel Cantu Jauregui
* @author angel.cantu@sie-gorup.net
* @link https:/www.sie-group.net
* @version 1.1
* @license GNU GENERAL PUBLIC LICENSE, Version 3, 29 June 2007
*
*/

class Syscom{
	/**
	* @var string api webservice syscom.mx
	*/
	const API= 'https://developers.syscom.mx';

	/**
	* @var string api webservice de pruebas syscom.mx (aun no tienen)
	*/
	const API_SANDBOX= 'https://developers.syscom.mx';

	/**
	* @var string ruta para autentificacion OAUTH
	*/
	const OAUTH= '/oauth/token';

	/**
	* @var string ruta para envio de consultas
	*/
	const SEND= '/api/v1/';

	/**
	* @var string variable para uso estatico, se debe indicar el client_id
	*/
	const USER_APP='';

	/**
	* @var string variable para uso estatico, se debe indicar el secret
	*/
	const SECRET_APP='';

	/**
	* @var string resguardo del usuario a utilizar
	*/
	private $user=NULL;

	/**
	* @var string resguardo del secret a utilizar
	*/
	private $secret=NULL;

	/**
	* @var string ruta al api a utilizar
	*/
	private $api=NULL;

	/**
	* @var string ruta al api de autentificacion a utilizar
	*/
	private $apiAuth=NULL;

	/**
	* @var string token a utilizar
	*/
	private $token=NULL;

	/**
	* @var boolean estado del sandbox
	*/
	private $sandbox=FALSE;

	/**
	* @var string mensaje de error
	*/
	private $error=NULL;

	/**
	* @var string codigo para el mensaje de error
	*/
	private $error_code=NULL;

	/**
	* @var string mensaje de exito
	*/
	private $sucess=NULL;

	/**
	* @var string codigo para el mensaje de exito
	*/
	private $sucess_code=NULL;

	/**
	* @var string mensaje de respuesta para consumo
	*/
	private $response=NULL;

	/**
	* @var string cabeceras que se envian
	*/
	private $headerRequest=NULL;

	/**
	* @var string cabeceras que recibimos como respuesta
	*/
	private $headerResponse=NULL;

	/**
	* @var array cabeceras para los distintos metodo a usar en el socket
	*/
	private $sendHeaders=NULL;

	/**
	* @var boolean modalidad de debug para visualziar cabeceras
	*/
	private $onDebug=false;

	/**
	* establece el nombre de usuario
	*
	* @param string $a username
	*/
	public function setUser($a=NULL) {
		$this->user= (self::USER_APP ? self::USER_APP : ($a ? $a:NULL));
	}

	/**
	* retorna el nombre de usuario o client_id
	*
	* @return string $a username
	*/
	public function getUser($a=NULL) {
		return $this->user;
	}

	/**
	* establece el secreto
	*
	* @param string $a secret
	*/
	public function setSecret($a=NULL) {
		$this->secret= (self::SECRET_APP ? self::SECRET_APP : ($a ? $a:NULL));
	}

	/**
	* retorn el secreto
	*
	* @return string $a secret
	*/
	public function getSecret($a=NULL) {
		return $this->secret;
	}

	/**
	* Retorna contenido de la variable $headerRequest
	*
	* @return string contenido de variable
	*/
	public function getHeaderRequest() {
		return $this->headerRequest;
	}

	/**
	* Establece el contenido del Request
	*
	* @param string $a contenido de variable
	*/
	public function setHeaderRequest($a=NULL) {
		$this->headerRequest= ($a ? $a:NULL);
	}

	/**
	* Retorna contenido de la variable $headerResponse
	*
	* @return string contenido de variable
	*/
	public function getHeaderResponse() {
		return $this->headerResponse;
	}

	/**
	* Establece el contenido del Response
	*
	* @param string $a contenido de variable
	*/
	public function setHeaderResponse($a=NULL) {
		$this->headerResponse= ($a ? $a:NULL);
	}

	/**
	* Establece el contenido de la variable $response
	*
	* @param string $a contenido de variable
	*/
	public function setRespuesta($a=NULL) {
		$this->response= ($a ? $a:NULL);
	}

	/**
	* Reotrna el contenido de la variable $response
	*
	* @return string $a contenido de variable
	*/
	public function getRespuesta($a=NULL) {
		return $this->response;
	}

	/**
	* Retorna contenido de la variable $error
	*
	* @return string contenido de variable
	*/
	public function getError() {
		return $this->error;
	}

	/**
	* Establece el contenido de la variable $error
	*
	* @param string $a contenido de variable
	* @param string $code codigo de error, opcional
	*/
	public function setError($a=NULL, $code=NULL) {
		$this->error= ($a ? $a:NULL);
		$this->setErrorCode($code ? $code:NULL);
	}

	/**
	* Retorna contenido de la variable $error_code
	*
	* @return string contenido de variable
	*/
	public function getErrorCode() {
		return $this->error_code;
	}

	/**
	* Establece el contenido de la variable $error_code
	*
	* @param string $a contenido de variable
	*/
	public function setErrorCode($a=NULL) {
		$this->error_code= ($a ? $a:NULL);
		if( !strcmp($a, "200") )	$this->setSucess($this->getError());
	}

	/**
	* Retorna contenido de la variable $sucess
	*
	* @return string contenido de variable
	*/
	public function getSucess() {
		return $this->sucess;
	}

	/**
	* Establece el contenido de la variable $sucess
	*
	* @param string $a contenido de variable
	* @param string $code codigo de error, opcional
	*/
	public function setSucess($a=NULL, $code=NULL) {
		$this->sucess= ($a ? $a:NULL);
		$this->setSucessCode($code ? $code:NULL);
	}

	/**
	* Establece el contenido de la variable $sucess_code
	*
	* @param string $a contenido de variable
	*/
	public function setSucessCode($a=NULL) {
		$this->sucess_code= ($a ? $a:NULL);
	}

	/**
	* Retorna el contenido de la variable $sucess_code
	*
	* @param string contenido de variable
	*/
	public function getSucessCode() {
		return $this->sucess_code;
	}

	/**
	* establece el webservice de autentificacion
	*/
	public function setApiAuth() {
		$this->apiAuth= ($this->getSandbox() ? self::API_SANDBOX : self::API).self::OAUTH;
	}

	/**
	* retorna el webservice de Auentificacion
	*
	* @return string url api
	*/
	public function getApiAuth() {
		return $this->apiAuth;
	}

	/**
	* establece el webservice de conexion
	*/
	public function setApi() {
		$this->api= ($this->getSandbox() ? self::API_SANDBOX : self::API).self::SEND;
	}

	/**
	* retorna el webservice de conexion
	*
	* @return string url api
	*/
	public function getApi() {
		return $this->api;
	}

	/**
	* establece una token existente o guardada en sistema
	*
	* @param string $a hash token
	*/
	public function setToken($a=NULL) {
		$this->token= ($a ? $a:NULL);
	}

	/**
	* retorna o crea una token
	*
	* @return string hash token
	*/
	public function getToken() {
		return $this->token;
	}

	/**
	* establece las cabeceras para en evio de datos por socket
	*
	* @param array $a arreglo de datos para la cebecera
	*/
	public function setSendHeaders($a=array()) {
		$this->sendHeaders= (is_array($a) ? $a:NULL);
	}

	/**
	* establece las cabeceras para en evio de datos por socket
	*
	* @return array $a arreglo de cabeceras
	*/
	public function getSendHeaders($a=array()) {
		return $this->sendHeaders;
	}

	/**
	* analizador de respuestas
	*/
	public function debugResponse() {
		if( $this->isDebugEnabled() ) { # habilitado el debug?
			echo "\n\n--- Request:\n";
			print_r($this->getHeaderRequest());
			echo "\n\n--- Response:\b";
			print_r($this->getHeaderResponse());
		}

		if( !$this->getHeaderResponse() )
			$this->setError("No se recibieron datos del servidor", "008");
		else { # datos con exito
			$r= explode("\r\n\r\n", $this->getHeaderResponse(), 2);

			if( strstr($r[0], "Continue") ) { # continua, el ID ya existe
				$y= explode("\r\n\r\n", $r[1], 2);
				$this->setSucess(json_decode($y[1]), "01");
			}
			else {
				$this->setSucess(json_decode($r[1]), "01");
			}
		}
	}

	/**
	* enviar la informacion a mercado libre
	*
	* @param string $onGet datos adicionales para el URL en GET
	* @param string $postData datos en json para el flujo POST
	* @param string $metodo flujo de datos para usarse con: PUT, DELETE y UPDATE
	*/
	public function sendToSyscom($onGet=NULL, $postData=NULL, $metodo=NULL) {
		$headers= $this->getSendHeaders(); # array("Accept-Encoding: gzip,deflate", "Content-Type: application/json")
		$dataGet= ($onGet ? $onGet:'');
		$dataSend= ($postData ? $postData:NULL);

		$s= curl_init();
		curl_setopt($s, CURLOPT_URL, $onGet );
		curl_setopt($s, CURLOPT_SSL_VERIFYPEER, true );
		curl_setopt($s, CURLOPT_USERAGENT, 'moneyBox-ERP' );
		curl_setopt($s, CURLOPT_HTTPHEADER, $headers );
		curl_setopt($s, CURLOPT_HEADER, 1 );
		
		if( $metodo ) 	curl_setopt($s, CURLOPT_CUSTOMREQUEST, $metodo );
		if( $dataSend ) {
			curl_setopt($s, CURLOPT_POST, 1);
			curl_setopt($s, CURLOPT_POSTFIELDS, $dataSend);
		}

		curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($s, CURLOPT_VERBOSE, TRUE);
		curl_setopt($s, CURLINFO_HEADER_OUT, true);
		$resp= curl_exec($s);
		$rq= curl_getinfo($s);
		$this->setHeaderRequest($rq["request_header"]); // request
		$this->setHeaderResponse($resp); // response
		$this->debugResponse();
		curl_close($s);
		unset($rq, $resp, $s);
	}

	/**
	* serializa datos para envio por GET
	*
	* @param array $a informacion en arreglo
	* @return string datos serializados
	*/
	public function serialization($a=NULL) {
		if( is_array($a) && count($a) ) {
			$aux='';
			foreach( $a as $key=>$val )
				$aux .= ($aux ? '&':''). $key. '='. $val;
			return $aux;
		}
	return 0;
	}

	/**
	* genera una token de conexion
	*/
	public function createToken() {
		if( !$this->getUser() ) 	$this->setError("no indico el usuario o client_id");
		else if( !$this->getSecret() ) 	$this->setError("no indico el secreto");
		else if( $this->getToken() ) 	$this->setError("ya existe token, no es necesario renovar");
		else {
			$header= array( 
				# "Accept-Encoding: gzip,deflate", 
				"Content-Type: application/x-www-form-urlencoded"
			);
			$param= array(
				"grant_type"=>"client_credentials", 
				"client_id"=>$this->getUser(), 
				"client_secret"=>$this->getSecret()
			);

			$this->setSendHeaders($header);
			$this->sendToSyscom($this->getApiAuth(), $this->serialization($param));

			if( !$this->getError() ) {
				$r= $this->getSucess();

				if( !$r )
					$this->setError("No se recibieron datos del servidor", "008");
				else {
					$this->setError(NULL);
					$this->setToken($r->access_token);
					$this->setRespuesta($r);
					$this->setSucess("Listo para utilizar, token: ". $this->getToken(), "01");
				}
			}
		}
	}

	/**
	* inicializa la conexion creando el token para envio de datos
	*/
	public function conectar() {
		if( !$this->getUser() ) 	$this->setError("no indico el usuario o client_id");
		else if( !$this->getSecret() ) 	$this->setError("no indico el secreto");
		else {
			if( !$this->getToken() ) # no existe token
				$this->createToken(); # crear token
			else {
				$this->setError(NULL);
				$this->setSucess("Listo para utilizar, token: ". $this->getToken(), "01");
			}
		}
	}

	/**
	* buscalas categorias
	*
	* @param string $a la categoria
	*/
	public function getCategoria($c=NULL) {
		if( !$this->getUser() ) 	$this->setError("no indico el usuario o client_id");
		else if( !$this->getSecret() ) 	$this->setError("no indico el secreto");
		else if( !$this->getToken() ) 	$this->setError("no existe token aun");
		else if( is_array($c) )	$this->setError( "debe indicar algo para la bsuqueda, ejm: \"get\"");
		else {
			$header= array( 
				# "Accept-Encoding: gzip,deflate", 
				"Authorization: Bearer ". $this->getToken()
			);
			$get= 'categorias'.($c ? $c:'');

			$this->setSendHeaders($header);
			$this->sendToSyscom($this->getApi().$get);

			if( !$this->getError() ) {
				$r= $this->getSucess();

				if( !$r )
					$this->setError("No se recibieron datos del servidor", "008");
				else {
					$this->setRespuesta($r);

					if( isset($r->code) ) # error
						$this->setError($r->detail, $r->code);
					else {
						$this->setError(NULL);
						$this->setSucess("Consulta generada con exito...", "01");
					}
				}
			}
		}
	}

	/**
	* busca productos apartir de parte del nombre y categoria, o solo alguno de ellos
	*
	* @param array $req arreglo de datos a enviar en get
	*/
	public function getProducto($req=NULL) {
		if( !$this->getUser() ) 	$this->setError("no indico el usuario o client_id");
		else if( !$this->getSecret() ) 	$this->setError("no indico el secreto");
		else if( !$this->getToken() ) 	$this->setError("no existe token aun");
		else if( !is_array($req) ) 	$this->setError("debe indicar almenos un nombre o numero de categoria para buscar");
		else {
			#$this->getCategoria($c);
			#if( !$this->getError() ) { # la categoria si existe
				$get= 'productos?';
				$header= array( 
					"Authorization: Bearer ". $this->getToken()
				);
				$params= $req;

				$this->setSendHeaders($header);
				$this->sendToSyscom($this->getApi().$get.$this->serialization($params), NULL, "GET");

				if( !$this->getError() ) {
					$r= $this->getSucess();

					if( !$r )
						$this->setError("No se recibieron datos del servidor", "008");
					else {
						$this->setRespuesta($r);

						if( isset($r->code) ) # error
							$this->setError($r->detail, $r->code);
						else {
							$this->setError(NULL);
							$this->setSucess("Consulta generada con exito...", "01");
						}
					}
				}
			#}
		}
	}

	/**
	* eliminar el slash del numero de categoria y retorna solo el numero
	*
	* @param string $a nombre de categoria con slash
	* @return string solo el numero de categoria
	*/
	public function cleanCategoria($a=NULL) {
		return (strstr($a, "/") ? substr($a, 1):$a);
	}

	/**
	* establece el estado del Sandbox para pruebas
	*
	* @param boolean $a estado
	*/
	public function setSandboxMode($a=false) {
		$this->sandbox= ($a ? true:false);
	}

	/**
	* retorna el estado del Sandbox
	*
	* @return boolean estado
	*/
	public function getSandbox() {
		return $this->sandbox;
	}

	/**
	* Habilitaciony deshabilitacion del modo Debug
	*
	* @param boolean $a estado
	*/
	public function setDebugMode($a=false) {
		$this->onDebug= ($a ? true:false);
	}

	/**
	* verificador del estado del modo Debug
	*
	* @return boolean estado del debug
	*/
	public function isDebugEnabled() {
		return ($this->onDebug ? true:false);;
	}

	/**
	* construccion de clase principal
	*
	* @param string $user el nombre de usuario o cliente_id
	* @param string $secret el secreto del app
	* @param boolean $sandbox estado del modo Sandbox
	* @param boolean $onDebug estado del modo Debug
	*/
	public function __construct($user=NULL, $secret=NULL, $sandbox=false, $onDebug=false) {
		$this->setSandboxMode($sandbox);
		$this->setDebugMode($onDebug);
		$this->setApiAuth(); # creamos ruta para autentificacion
		$this->setApi(); # creamos ruta de conexion
		$this->setUser($user);
		$this->setSecret($secret);
		$this->conectar();
	}
}
?>